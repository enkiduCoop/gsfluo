#!/usr/bin/php
<?php

require_once('gsfluo.php');


$fluourl = '';

# gs-ensalutiloj

$api_url = ''; // ekzemple http://lamatriz.org/api/statuses/update.xml
$salutnomo = '';
$pasvorto = '';


// kreo de la RssLegilo al kiu oni informas pri la uzota rssfluo

$rss_legilo = new RssLegilo($fluourl);


// Legado de novaj elementoj en la rssfluo

$pepoj = $rss_legilo->legi();

// Kreo de GsKonektilo al kiu oni disponigas gs-ensalutilojn

$gs_konektilo = new GsKonektilo($api_url, $salutnomo, $pasvorto);

// Afiŝado de novaj pepoj trovitaj en la lasta legado de la rssfluo

foreach ($pepoj as $pepo) {

    $gs_konektilo->afishi($pepo->titolo, $pepo->ligilo, $pepo->priskribo, $pepo->kategorioj);
    
}

// Kaj laste oni ĝisdatigas la daton de la lasta afiŝita pepo
$rss_legilo->ghisdatigi_daton();
